/*
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */
/**
 * Modified UDP-client for IEEE key management
 *
 * Runar Mar Magnusson <rmma@kth.se>
 */

#include "contiki.h"
#include "contiki-lib.h"
#include "contiki-net.h"
#include "net/ip/uip.h"
#include "net/ipv6/uip-ds6.h"
#include "net/ip/uip-udp-packet.h"

#include "common-ipsec.h"
#include "ike.h"
#include "spd.h"
#include "machine.h"
#include "ieee-802-15-4-sad.h"
#include "ieee-802-15-4-sa.h"

#include <string.h>
#include <stdbool.h>

#define DEBUG DEBUG_PRINT
#include "net/ip/uip-debug.h"

#define SEND_INTERVAL   10 * CLOCK_SECOND
#define MAX_PAYLOAD_LEN   40

static struct uip_udp_conn *client_conn;
static uip_ipaddr_t server_ipaddr;
/*---------------------------------------------------------------------------*/
PROCESS(udp_client_process, "UDP client process");
#if WITH_OPENMOTE
#include "flash-erase.h"
AUTOSTART_PROCESSES(&udp_client_process, &flash_erase_process);
#else
AUTOSTART_PROCESSES(&udp_client_process);
#endif
/*---------------------------------------------------------------------------*/
static void
print_local_addresses(void)
{
  int i;
  uint8_t state;

  PRINTF("Client IPv6 addresses: ");
  for(i = 0; i < UIP_DS6_ADDR_NB; i++) {
    state = uip_ds6_if.addr_list[i].state;
    if(uip_ds6_if.addr_list[i].isused &&
       (state == ADDR_TENTATIVE || state == ADDR_PREFERRED)) {
      PRINT6ADDR(&uip_ds6_if.addr_list[i].ipaddr);
      PRINTF("\n");
    }
  }
}
/*---------------------------------------------------------------------------*/
#if UIP_CONF_ROUTER
static void
set_global_address(void)
{
  uip_ipaddr_t ipaddr, server_next;

  uip_ip6addr(&ipaddr, 0xaaaa, 0, 0, 0, 0, 0, 0, 0);
  uip_ds6_set_addr_iid(&ipaddr, &uip_lladdr);
  uip_ds6_addr_add(&ipaddr, 0, ADDR_AUTOCONF);

  uip_ip6addr(&server_ipaddr, 0xaaaa, 0, 0, 0, 0x0202, 0x0002, 0x0002, 0x0002);
  uip_ip6addr(&server_next, 0xfe80, 0, 0, 0, 0x0202, 0x0002, 0x0002, 0x0002);
  uip_ds6_route_add(&server_ipaddr, 128, &server_next);
#endif /* UIP_CONF_ROUTER */
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(udp_client_process, ev, data)
{
  static struct etimer et;

  PROCESS_BEGIN();
  PRINTF("UDP client process started\n");

  etimer_set(&et, 30 * CLOCK_CONF_SECOND);

#if UIP_CONF_ROUTER
  set_global_address();
#endif

  print_local_addresses();

  spd_conf_init();
  ieee_sad_init();
  PRINTF("SAD and SPD initialized\n");
  ike_init();
  PRINTF("IKEv2 service initialized\n");

  /* Set the packet descriptor for the traffic*/
  ipsec_addr_t packet_tag;
  packet_tag.my_port = 0;
  packet_tag.peer_port = 0;
  packet_tag.nextlayer_proto = SPD_SELECTOR_NL_ANY_PROTOCOL;
  memcpy(&packet_tag.peer_addr, &server_ipaddr, sizeof(uip_ip6addr_t));

  spd_entry_t *spd_entry = spd_get_entry_by_addr(&packet_tag, SA_PROTO_IEEE_802_15_4);

  /* check if there is an SAD entry matching the packet tag */
  if(spd_entry != NULL) {
    printf("SPD entry found for traffic with Action to ");
    switch(spd_entry->proc_action) {
    case SPD_ACTION_BYPASS:
      printf("BYPASS");
      return 0;
    case SPD_ACTION_PROTECT:
      printf("PROTECT");
      break;
    case SPD_ACTION_DISCARD:
      printf("DISCARD");
      break;
    }
    printf("\n");
  }

  ike_arg_packet_tag = packet_tag;
  process_post(&ike2_service, ike_negotiate_event, (void *)spd_entry);
  static uip_lladdr_t server_ll;

  while(1) {
    PROCESS_WAIT_EVENT();
    if(ev == ike_negotiate_done) {
      printf("IKE Negotiation done event Client\n");
      ike_statem_session_t *session = (ike_statem_session_t *)data;
      printf("Session %p\n", session);
      printf("Outgoing SAD entry\n");
      PRINTIEEESADENTRY(session->outgoing_entry);
      printf("Incoming SAD entry\n");
      PRINTIEEESADENTRY(session->incoming_entry);
      memcpy(&server_ll, &session->peer_lladdr, sizeof(uip_lladdr_t));
    } else if(ev == PROCESS_EVENT_TIMER) {
      etimer_reset(&et);

      ieee_sad_entry_t *incoming_sad_entry, *outgoing_sad_entry;
      incoming_sad_entry = outgoing_sad_entry = NULL;

      /* Look for and outgoing SA for RPL in SAD*/
      outgoing_sad_entry = ieee_sad_get_outgoing_entry(&server_ll);
      if(outgoing_sad_entry == NULL) {
        printf("Could not find outgoing SAD entry\n");
      } else {
        printf("Found Outgoing SAD entry\n");
        printf("key for outgoing traffic: ");
        uint8_t i;
        for(i = 0; i < IKE_IEEE_KEY_LEN; i++) {
          if(get_encr_ieee_keymat_len(outgoing_sad_entry->sa.encr) == 0) {
            /* Print out Integrity transform key */
            printf("%02X ", (outgoing_sad_entry)->sa.sk_a[i]);
          } else {
            /* Print out Encryption transform key */
            printf("%02X ", (outgoing_sad_entry)->sa.sk_e[i]);
          }
        }
        printf("\n");
      }

      /* Look for and outgoing SA for RPL in SAD*/
      incoming_sad_entry = ieee_sad_get_incoming_entry(&server_ll);
      if(incoming_sad_entry == NULL) {
        printf("Could not find incoming SAD entry\n");
      } else {
        printf("Found Incoming SAD entry\n");
        printf("key for incoming traffic: ");
        uint8_t i;
        for(i = 0; i < IKE_IEEE_KEY_LEN; i++) {
          if(get_encr_ieee_keymat_len(incoming_sad_entry->sa.encr) == 0) {
            /* Print out Integrity transform key */
            printf("%02X ", (incoming_sad_entry)->sa.sk_a[i]);
          } else {
            /* Print out Encryption transform key */
            printf("%02X ", (incoming_sad_entry)->sa.sk_e[i]);
          }
        }
        printf("\n");
      }
    }
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
