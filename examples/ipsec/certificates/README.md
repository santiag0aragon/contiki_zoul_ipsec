Setting up certificates on strongswan 

https://wiki.strongswan.org/projects/strongswan/wiki/SimpleCA - Tutorial

Generating the  keys and certificate for the CA
Create the ca private key
pki --gen --type ecdsa --size 256 > caKey.der (192 bit keys not supported)

Generate a self signed certificate for the CA
ipsec pki --self --in caKey.der --type ecdsa --dn "CN=SICS TEST CA" --ca > caCert.der

Create the keys and certificate for the server
Create the server private key
pki --gen --type ecdsa --size 256 > serverKey.der

Issue a certificate for the server using the CA
ipsec pki --pub --type ecdsa --in serverKey.der | ipsec pki --issue --cacert caCert.der --cakey caKey.der \
--dn "CN=Strongswan Server" > serverCert.der

Create the keys and certificate for the OpenMote client/server
Create the private key
pki --gen --type ecdsa --size 256 > openmoteKey.der

Issue a certificate for the server using the CA
ipsec pki --pub --type ecdsa --in openmoteKey.der | ipsec pki --issue --cacert caCert.der --cakey caKey.der \
--dn "CN=OpenMote" > openmoteCert.der

Move the server certificate so strongswan can use it 
sudo cp serverKey.der /etc/ipsec.d/private/
sudo cp serverCert.der /etc/ipsec.d/certs/
sudo cp caCert.der /etc/ipsec.d/cacerts/

Update the  secrets file 
Add the line 
ECDSA: serverKey.der 
to /etc/ipsec.secrets with a text editor

Restart the strongswan server
sudo ipsec rereadsecrets
sudo ipsec restart 


Converting certificates and keys to PEM from DER
It can be good idea to have a .pem encoding of the certificates

Converting Certificates from .DER to .PEM
openssl x509 -inform der -in serverCert.der -out serverCert.pem
openssl x509 -inform der -in caCert.der -out caCert.pem
openssl x509 -inform der -in openmoteCert.der -out openmoteCert.pem

Converting the private Key from .DER to .PEM
openssl ec -inform der -in serverKey.der -out serverKey.pem
openssl ec -inform der -in caKey.der -out caKey.pem
openssl ec -inform der -in openmoteKey.der -out openmoteKey.pem

Extracting the public key from a .PEM certificate
openssl x509 -pubkey -noout -in serverCert.pem  > serverPubKey.pem
openssl x509 -pubkey -noout -in caCert.pem  > caPubKey.pem
openssl x509 -pubkey -noout -in openmoteCert.pem  > openmotePubKey.pem


