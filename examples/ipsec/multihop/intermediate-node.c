/*
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */
/**
 * Code for intermediate nodes between the UDP server and client
 */

#include "contiki.h"
#include "contiki-lib.h"
#include "contiki-net.h"
#include "net/ip/uip.h"
#include "net/ipv6/uip-ds6.h"
#include "net/ip/uip-udp-packet.h"

#include <string.h>
#include <stdbool.h>

#if WITH_COMPOWER
#include "powertrace.h"
#endif

#define DEBUG_PRINT
#include "net/ip/uip-debug.h"

#ifndef NODE_ID
#define NODE_ID 3
#endif

#ifndef NR_NODES
#define NR_NODES 1
#endif

uint8_t nodeid = NODE_ID;
uint8_t nr_nodes = NR_NODES;

static struct uip_udp_conn *client_conn;
static uip_ipaddr_t server_ipaddr;

/*---------------------------------------------------------------------------*/
PROCESS(intermediate_node_process, "Intermediate node process");
#if WITH_OPENMOTE
#include "flash-erase.h"
AUTOSTART_PROCESSES(&intermediate_node_process, &flash_erase_process);
#else
AUTOSTART_PROCESSES(&intermediate_node_process);
#endif
/*---------------------------------------------------------------------------*/
static void
print_local_addresses(void)
{
  int i;
  uint8_t state;

  PRINTF("Client IPv6 addresses: ");
  for(i = 0; i < UIP_DS6_ADDR_NB; i++) {
    state = uip_ds6_if.addr_list[i].state;
    if(uip_ds6_if.addr_list[i].isused &&
       (state == ADDR_TENTATIVE || state == ADDR_PREFERRED)) {
      PRINT6ADDR(&uip_ds6_if.addr_list[i].ipaddr);
      PRINTF("\n");
    }
  }
}
/*---------------------------------------------------------------------------*/
#if UIP_CONF_ROUTER
static void
set_global_address(void)
{
  uip_ipaddr_t ipaddr;

  uip_ip6addr(&ipaddr, 0xaaaa, 0, 0, 0, 0, 0, 0, 0);
  uip_ds6_set_addr_iid(&ipaddr, &uip_lladdr);
  uip_ds6_addr_add(&ipaddr, 0, ADDR_AUTOCONF);
}
#endif /* UIP_CONF_ROUTER */
static void
add_routes(void)
{
  /* Client <-> 1st <-> 2nd <-> 3rd <-> Server */
  uip_ipaddr_t ipaddr_link_server, ipaddr_link_client,
               ipaddr_global_server, ipaddr_global_client,
               ipaddr_link_node3, ipaddr_link_node4, ipaddr_link_node5,
               ipaddr_client_next_hop, ipaddr_server_next_hop;

#if WITH_COOJA
  /* Cooja nodes */
  /* Set the global addresses of the server and client */
  uip_ip6addr(&ipaddr_global_server, 0xaaaa, 0, 0, 0, 0x0201, 0x0001, 0x0001, 0x0001);
  uip_ip6addr(&ipaddr_global_client, 0xaaaa, 0, 0, 0, 0x0202, 0x0002, 0x0002, 0x0002);

  /* Set link local addresses */
  uip_ip6addr(&ipaddr_link_server, 0xfe80, 0, 0, 0, 0x0201, 0x0001, 0x0001, 0x0001);
  uip_ip6addr(&ipaddr_link_client, 0xfe80, 0, 0, 0, 0x0202, 0x0002, 0x0002, 0x0002);
  uip_ip6addr(&ipaddr_link_node3, 0xfe80, 0, 0, 0, 0x0203, 0x0003, 0x0003, 0x0003);
  uip_ip6addr(&ipaddr_link_node4, 0xfe80, 0, 0, 0, 0x0204, 0x0004, 0x0004, 0x0004);
  uip_ip6addr(&ipaddr_link_node5, 0xfe80, 0, 0, 0, 0x0205, 0x0005, 0x0005, 0x0005);

  /* Set the link-layer addresses */
  uip_lladdr_t next_hop1 = { { 0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x01 } }; /* Server */
  uip_lladdr_t next_hop2 = { { 0x00, 0x02, 0x00, 0x02, 0x00, 0x02, 0x00, 0x02 } }; /* Client */
  uip_lladdr_t next_hop3 = { { 0x00, 0x03, 0x00, 0x03, 0x00, 0x03, 0x00, 0x03 } }; /* 1st Intermediate-node */
  uip_lladdr_t next_hop4 = { { 0x00, 0x04, 0x00, 0x04, 0x00, 0x04, 0x00, 0x04 } }; /* 2nd Intermediate-node */
  uip_lladdr_t next_hop5 = { { 0x00, 0x05, 0x00, 0x05, 0x00, 0x05, 0x00, 0x05 } }; /* 3rd Intermediate-node */
#else
  /* Real nodes */
  /* Set the global addresses of the server and client */
  uip_ip6addr(&ipaddr_global_server, 0xaaaa, 0, 0, 0, 0x0212, 0x4b00, 0x060d, 0x9b2b);
  uip_ip6addr(&ipaddr_global_client, 0xaaaa, 0, 0, 0, 0x0212, 0x4b00, 0x060d, 0x9b19);

  /* Set link local addresses */
  uip_ip6addr(&ipaddr_link_server, 0xfe80, 0, 0, 0, 0x0212, 0x4b00, 0x060d, 0x9b2b);
  uip_ip6addr(&ipaddr_link_client, 0xfe80, 0, 0, 0, 0x0212, 0x4b00, 0x060d, 0x9b19);
  uip_ip6addr(&ipaddr_link_node3, 0xfe80, 0, 0, 0, 0x0212, 0x4b00, 0x060d, 0x9b09);
  uip_ip6addr(&ipaddr_link_node4, 0xfe80, 0, 0, 0, 0x0212, 0x4b00, 0x060d, 0x9b13);
  uip_ip6addr(&ipaddr_link_node5, 0xfe80, 0, 0, 0, 0x0212, 0x4b00, 0x060d, 0x9af8);

  /* Set the link-layer addresses */
  uip_lladdr_t next_hop1 = { { 0x00, 0x12, 0x4b, 0x00, 0x06, 0x0d, 0x9b, 0x2b } }; /* Server */
  uip_lladdr_t next_hop2 = { { 0x00, 0x12, 0x4b, 0x00, 0x06, 0x0d, 0x9b, 0x19 } }; /* Client */
  uip_lladdr_t next_hop3 = { { 0x00, 0x12, 0x4b, 0x00, 0x06, 0x0d, 0x9b, 0x09 } }; /* 1st Intermediate-node */
  uip_lladdr_t next_hop4 = { { 0x00, 0x12, 0x4b, 0x00, 0x06, 0x0d, 0x9b, 0x13 } }; /* 2nd Intermediate-node */
  uip_lladdr_t next_hop5 = { { 0x00, 0x12, 0x4b, 0x00, 0x06, 0x0d, 0x9a, 0xf8 } }; /* 3rd Intermediate-node */
#endif
  if(nr_nodes != 0) {
    switch(nodeid) {
    case 3:
      uip_ds6_nbr_add(&ipaddr_link_client, &next_hop2, 0, NBR_STALE);
      uip_ipaddr_copy(&ipaddr_client_next_hop, &ipaddr_link_client);

      if(nr_nodes > 1) {
        uip_ds6_nbr_add(&ipaddr_link_node4, &next_hop4, 0, NBR_STALE);
        uip_ipaddr_copy(&ipaddr_server_next_hop, &ipaddr_link_node4);
      } else {
        uip_ds6_nbr_add(&ipaddr_link_server, &next_hop1, 0, NBR_STALE);
        uip_ipaddr_copy(&ipaddr_server_next_hop, &ipaddr_link_server);
      }
      break;
    case 4:
      uip_ds6_nbr_add(&ipaddr_link_node3, &next_hop3, 0, NBR_STALE);
      uip_ipaddr_copy(&ipaddr_client_next_hop, &ipaddr_link_node3);

      if(nr_nodes > 2) {
        uip_ds6_nbr_add(&ipaddr_link_node5, &next_hop5, 0, NBR_STALE);
        uip_ipaddr_copy(&ipaddr_server_next_hop, &ipaddr_link_node5);
      } else {
        uip_ds6_nbr_add(&ipaddr_link_server, &next_hop1, 0, NBR_STALE);
        uip_ipaddr_copy(&ipaddr_server_next_hop, &ipaddr_link_server);
      }
      break;
    case 5:
      uip_ds6_nbr_add(&ipaddr_link_node4, &next_hop4, 0, NBR_STALE);
      uip_ipaddr_copy(&ipaddr_client_next_hop, &ipaddr_link_node4);
      uip_ds6_nbr_add(&ipaddr_link_server, &next_hop1, 0, NBR_STALE);
      uip_ipaddr_copy(&ipaddr_server_next_hop, &ipaddr_link_server);
      break;
    }

    /* Add the static routes */
    uip_ds6_route_add(&ipaddr_global_server, 128, &ipaddr_server_next_hop);
    uip_ds6_route_add(&ipaddr_global_client, 128, &ipaddr_client_next_hop);
  }
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(intermediate_node_process, ev, data)
{
#if WITH_COMPOWER
  static int print = 0;
#endif

  PROCESS_BEGIN();

  PROCESS_PAUSE();

#if UIP_CONF_ROUTER
  set_global_address();
#endif

  PRINTF("Intermediate node process started\n");

  print_local_addresses();

  add_routes();

  while(1) {
    PROCESS_YIELD();
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
