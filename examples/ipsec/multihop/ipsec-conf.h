#ifndef __IPSEC_PROJECT_H__
#define __IPSEC_PROJECT_H__

/* #include "project-conf.h" */

/* / * CC2538 config * / */
#ifndef ENERGEST_CONF_ON
#define ENERGEST_CONF_ON            1 /**< Energest Module */
#endif

#ifndef LPM_CONF_ENABLE
#define LPM_CONF_ENABLE       1 /**< Set to 0 to disable LPM entirely */
#endif

#ifndef LPM_CONF_MAX_PM
#define LPM_CONF_MAX_PM       1 /* was 1 */
#endif

#ifndef LPM_CONF_STATS
#define LPM_CONF_STATS        1 /**< Set to 1 to enable LPM-related stats */
#endif

/* / * Extra uIP logging * / */
#undef UIP_CONF_LOGGING
#define UIP_CONF_LOGGING 0

#ifdef UIP_CONF_BUFFER_SIZE
#undef UIP_CONF_BUFFER_SIZE
#endif
#define UIP_CONF_BUFFER_SIZE  1280 /* This option can be set in various platform specific header files as well */

#define WATCHDOG_CONF_ENABLE 0 /**< Disable the watchdog timer */

#define CC2538_RF_CONF_CHANNEL 15
#define NETSTACK_CONF_RDC nullrdc_driver

#if WITH_IPSEC

#define IKE_SAD_ENTRIES 5

#define IKE_SESSION_NUM 5

/* Enable IPSEC debugging */
#define IPSEC_DEBUG 0
#define DEBUG 0

#ifndef IKE_IPSEC_INFO
#define IKE_IPSEC_INFO 0
#endif
#ifndef IPSEC_TIME_STATS
#define IPSEC_TIME_STATS 0
#endif

#define CONTIKI_WITH_IPV6       1
#define NETSTACK_CONF_WITH_IPV6 1

/* IPsec configuration ------------------*/
/* Enabling ESP is equal to enabling to IPsec. Note that AH is not supported! */
#define WITH_CONF_IPSEC_ESP             1
/* */
/* The IKE subsystem is optional if the SAs are manually configured */
#define WITH_CONF_IPSEC_IKE             1
/*  
 * Manual SA configuration allows you as developer to create persistent SAs in the SAD. 
 * This is probably what you want to use if WITH_CONF_IPSEC_IKE is set 0, but please note 
 * that both features can be used simultaneously on a host as per the IPsec RFC. 
 * 
 * The manual SAs can be set in the function sad_conf() 
 */
#define WITH_CONF_MANUAL_SA   0

/*
 * Enabling certificate authentication for the IKE AUTH payload
 * If the responder uses certificate authentication
 */
#define WITH_CONF_IKE_CERT_AUTH 0

/* Variables that need to be defined when using certificates */

/* Subject Public Key Info Element of the CA in X.509 base64 encoding
 * generated with:
 *  openssl x509 -pubkey -noout -in caCert.pem  > caPubKey.pem
 */
#define CERT_KEY_AUTHORITY { 0x30, 0x59, 0x30, 0x13, 0x06, 0x07, 0x2A, 0x86, 0x48, \
                             0xCE, 0x3D, 0x02, 0x01, 0x06, 0x08, 0x2A, 0x86, 0x48, 0xCE, \
                             0x3D, 0x03, 0x01, 0x07, 0x03, 0x42, 0x00, 0x04, 0x16, 0xCB, \
                             0x72, 0xE9, 0x61, 0x8C, 0x6C, 0x02, 0x0E, 0xA8, 0xC1, 0x8F, \
                             0x32, 0x33, 0xA3, 0x95, 0x3A, 0x3F, 0xB5, 0x58, 0x59, 0x71, \
                             0x22, 0x2E, 0x1C, 0x44, 0x4B, 0x44, 0x16, 0x28, 0x92, 0x69, \
                             0xBA, 0x86, 0xE8, 0xE2, 0x9C, 0x2E, 0x58, 0x03, 0xA3, 0xA4, \
                             0x9F, 0x3A, 0x2B, 0xBC, 0xBA, 0x66, 0x69, 0xF6, 0xD9, 0x30, \
                             0xE2, 0x55, 0x26, 0xBB, 0x77, 0x80, 0x70, 0x40, 0x52, 0x83, \
                             0x67, 0xDE }
#if WITH_CONF_IKE_CERT_AUTH

/* Client certificate pem encoded in hex */
#define CLIENT_CERT { 0x30, 0x82, 0x01, 0x44, 0x30, 0x81, 0xEB, 0xA0, 0x03, 0x02, 0x01, 0x02, 0x02, 0x08, 0x6F, 0x51, 0xFF, 0x8D, 0x8C, 0x54, 0x5A, 0xE9, 0x30, 0x0A, 0x06, 0x08, 0x2A, 0x86, 0x48, 0xCE, 0x3D, 0x04, 0x03, 0x02, 0x30, 0x17, 0x31, 0x15, 0x30, 0x13, 0x06, 0x03, 0x55, 0x04, 0x03, 0x13, 0x0C, 0x53, 0x49, 0x43, 0x53, 0x20, 0x54, 0x45, 0x53, 0x54, 0x20, 0x43, 0x41, 0x30, 0x1E, 0x17, 0x0D, 0x31, 0x35, 0x30, 0x36, 0x31, 0x35, 0x30, 0x37, 0x35, 0x33, 0x32, 0x33, 0x5A, 0x17, 0x0D, 0x31, 0x38, 0x30, 0x36, 0x31, 0x34, 0x30, 0x37, 0x35, 0x33, 0x32, 0x33, 0x5A, 0x30, 0x15, 0x31, 0x13, 0x30, 0x11, 0x06, 0x03, 0x55, 0x04, 0x03, 0x13, 0x0A, 0x55, 0x44, 0x50, 0x20, 0x53, 0x65, 0x72, 0x76, 0x65, 0x72, 0x30, 0x59, 0x30, 0x13, 0x06, 0x07, 0x2A, 0x86, 0x48, 0xCE, 0x3D, 0x02, 0x01, 0x06, 0x08, 0x2A, 0x86, 0x48, 0xCE, 0x3D, 0x03, 0x01, 0x07, 0x03, 0x42, 0x00, 0x04, 0x3E, 0x4A, 0x25, 0xE3, 0xBF, 0xF3, 0x46, 0x3B, 0x0E, 0x3A, 0x06, 0xDB, 0xF4, 0x2D, 0xE2, 0xAE, 0x69, 0x4E, 0xA5, 0x84, 0x22, 0xF8, 0x15, 0xF5, 0x58, 0xA5, 0x59, 0x79, 0xDC, 0x6D, 0xC6, 0x5B, 0xCC, 0x12, 0x98, 0xC8, 0xBD, 0xD9, 0x49, 0x34, 0x6A, 0x1B, 0x4C, 0xCE, 0xE8, 0x63, 0x40, 0x37, 0x6B, 0x7A, 0x00, 0x7C, 0x2C, 0xB5, 0xD7, 0x6B, 0x29, 0x08, 0x1B, 0x76, 0x08, 0x66, 0x47, 0x51, 0xA3, 0x23, 0x30, 0x21, 0x30, 0x1F, 0x06, 0x03, 0x55, 0x1D, 0x23, 0x04, 0x18, 0x30, 0x16, 0x80, 0x14, 0x4C, 0x3C, 0x38, 0x59, 0xEA, 0x14, 0xF1, 0xD9, 0xFB, 0x9D, 0x1A, 0x2B, 0x25, 0xDD, 0x43, 0x60, 0x30, 0xE2, 0x54, 0x88, 0x30, 0x0A, 0x06, 0x08, 0x2A, 0x86, 0x48, 0xCE, 0x3D, 0x04, 0x03, 0x02, 0x03, 0x48, 0x00, 0x30, 0x45, 0x02, 0x20, 0x62, 0x58, 0x9E, 0xB3, 0xD2, 0x64, 0xEA, 0x6E, 0x38, 0x03, 0xC8, 0x6D, 0x07, 0xA5, 0x6D, 0x27, 0xE8, 0xFA, 0xB4, 0xEF, 0x2B, 0x21, 0xCA, 0x4B, 0xE7, 0xD9, 0x18, 0xD7, 0x26, 0x46, 0x48, 0x07, 0x02, 0x21, 0x00, 0xBF, 0x69, 0x4A, 0xEF, 0xEC, 0xBB, 0xDB, 0xC1, 0xEB, 0xF3, 0xC2, 0x53, 0xC0, 0xA8, 0x63, 0x93, 0x77, 0xD0, 0x51, 0x5D, 0x4E, 0x8A, 0x90, 0x10, 0xB0, 0x74, 0x61, 0xA4, 0xC5, 0x50, 0x34, 0x93 }

/* Client private key pem encoded in hex*/
#define CLIENT_PRIVATE_CERT_KEY { 0x30, 0x77, 0x02, 0x01, 0x01, 0x04, 0x20, 0x4E, 0x0C, 0xD6, 0x0E, 0xC5, 0xC3, 0x1A, 0x34, 0xEC, 0x0E, 0xC7, 0x8A, 0x92, 0x89, 0xB4, 0x00, 0x79, 0x04, 0x0A, 0x2F, 0x88, 0xF0, 0x72, 0xD2, 0x10, 0x80, 0xFB, 0xD5, 0x2A, 0xB5, 0x0D, 0x7F, 0xA0, 0x0A, 0x06, 0x08, 0x2A, 0x86, 0x48, 0xCE, 0x3D, 0x03, 0x01, 0x07, 0xA1, 0x44, 0x03, 0x42, 0x00, 0x04, 0x3E, 0x4A, 0x25, 0xE3, 0xBF, 0xF3, 0x46, 0x3B, 0x0E, 0x3A, 0x06, 0xDB, 0xF4, 0x2D, 0xE2, 0xAE, 0x69, 0x4E, 0xA5, 0x84, 0x22, 0xF8, 0x15, 0xF5, 0x58, 0xA5, 0x59, 0x79, 0xDC, 0x6D, 0xC6, 0x5B, 0xCC, 0x12, 0x98, 0xC8, 0xBD, 0xD9, 0x49, 0x34, 0x6A, 0x1B, 0x4C, 0xCE, 0xE8, 0x63, 0x40, 0x37, 0x6B, 0x7A, 0x00, 0x7C, 0x2C, 0xB5, 0xD7, 0x6B, 0x29, 0x08, 0x1B, 0x76, 0x08, 0x66, 0x47, 0x51 }

#endif

/* Shared secret to use when not using certificate authentication */
/* Shared secret defined below used in the IKE_AUTH payload */
#define SHARED_IKE_SECRET "aa280649dc17aa821ac305b5eb09d445"

/* ID up to 16 bytes*/
#define IKE_ID "test@sics.se"

/* IKE SA Configuration */
/* Integrity transform to use for the IKE SA, supported transforms:
   SA_INTEG_AES_XCBC_MAC_96, SA_INTEG_HMAC_SHA1_96, SA_INTEG_HMAC_SHA2_256_128
   NOTE: don't define if IKE_ENCR = SA_ENCR_CCM_{8,12,16}*/
/* #define IKE_INTEG SA_INTEG_AES_XCBC_MAC_96 */

/* Encryption transform to use for the IKE SA, supported transforms:
   SA_ENCR_AES_CTR, SA_ENCR_AES_CCM_8, SA_ENCR_AES_CCM_12, SA_ENCR_AES_CCM_16 */
#define IKE_ENCR SA_ENCR_AES_CCM_16

/* Psuedo-random function to use for the IKE SA, supported functions:
   SA_PRF_HMAC_SHA1, SA_PRF_HMAC_SHA2_256*/
#define IKE_PRF SA_PRF_HMAC_SHA2_256

/* Diffie-Hellman groups supported for IKE:
   SA_DH_192_RND_ECP_GROUP, SA_DH_256_RND_ECP_GROUP, SA_DH_256_BP_GROUP
   NOTE: When certificate authentication is used this defaults to
   SA_DH_256_RND_ECP_GROUP also this parameter must match CURVE_PARAMS from
   the makefile, that is
   SA_DH_192_RND_ECP_GROUP => CURVE_PARAMS = SECP192R1
   SA_DH_256_RND_ECP_GROUP => CURVE_PARAMS = SECP256R1 (Only supported ECDSA algorithm)
   SA_DH_256_BP_GROUP      => CURVE_PARAMS = BPOOLP256R1
 */
#define IKE_DH SA_DH_192_RND_ECP_GROUP

/* RPL SA Configuration */
/* Supported transforms in rpl-sa.h*/
#define ESP_ENCR  SA_ENCR_AES_CCM_16

/* Supported transforms, same as for IKE SA
   NOTE: don't define if ESP_ENCR=SA_ENCR_AES_CCM_{8,12,16}*/
/* #define ESP_INTEG SA_INTEG_AES_XCBC_MAC_96 */

/**
 * Configuring an AES implementation
 *
 * The only current implementation is that provided by the MIRACL -library. In the future
 * this can be extended with an interface to the CC2420 radio module which is equipped with
 * an AES hardware implementation.
 */
#ifdef HW_AES
#define CRYPTO_CONF_AES cc2538_aes
#else
#define CRYPTO_CONF_AES contiki_aes
#endif

/* ECC */
/* #define WITH_IPSEC_ESP 1 */
#define WORDS_32_BITS 1

#endif

/* #define QUEUEBUF_CONF_NUM 12 */

#endif