#!/bin/bash
#make TARGET=openmote clean all
make clean udp-client TARGET=openmote WITH_COMPOWER=1 
sudo python ../../../tools/cc2538-bsl/cc2538-bsl.py -p /dev/ttyUSB1 -e -w -v udp-client.bin
make clean udp-server TARGET=openmote WITH_IKE_INFO=1
sudo python ../../../tools/cc2538-bsl/cc2538-bsl.py -p /dev/ttyUSB0 -e -w -v udp-server.bin
sudo putty -load Contiki & sudo putty -load Contiki2
