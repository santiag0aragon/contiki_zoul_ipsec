/*
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

#include "contiki.h"
#include "contiki-lib.h"
#include "contiki-net.h"
#include "net/ip/uip.h"
#include "net/rpl/rpl.h"
#include "net/netstack.h"

#include <string.h>

#define DEBUG DEBUG_NONE
#include "net/ip/uip-debug.h"
#include <stdio.h>
#define PRINTF(...) printf(__VA_ARGS__)

/* #include "common-ipsec.h" */
#include "ike.h"
#include "spd.h"
#include "machine.h"

#include "rpl-sad.h"

#define UIP_IP_BUF   ((struct uip_ip_hdr *)&uip_buf[UIP_LLH_LEN])

#define MAX_PAYLOAD_LEN 120
PROCESS(udp_server_process, "UDP server process");

#if WITH_OPENMOTE
#include "flash-erase.h"
AUTOSTART_PROCESSES(&udp_server_process, &flash_erase_process);
#else
AUTOSTART_PROCESSES(&udp_server_process);
#endif

static struct uip_udp_conn *server_conn;

/*---------------------------------------------------------------------------*/
static void
print_local_addresses(void)
{
  int i;
  uint8_t state;

  PRINTF("Server IPv6 addresses: ");
  for(i = 0; i < UIP_DS6_ADDR_NB; i++) {
    state = uip_ds6_if.addr_list[i].state;
    if(uip_ds6_if.addr_list[i].isused &&
       (state == ADDR_TENTATIVE || state == ADDR_PREFERRED)) {
      PRINT6ADDR(&uip_ds6_if.addr_list[i].ipaddr);
      PRINTF("\n");
    }
  }
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(udp_server_process, ev, data)
{
#if UIP_CONF_ROUTER
  uip_ipaddr_t ipaddr;
#endif /* UIP_CONF_ROUTER */
  struct uip_ds6_addr *root_if;

  PROCESS_BEGIN();
  PRINTF("UDP server started\n");

#if UIP_CONF_ROUTER
  uip_ipaddr_t client_ipaddr, client_next;
  uip_ip6addr(&client_ipaddr, 0xaaaa, 0, 0, 0, 0x0201, 0x0001, 0x0001, 0x0001);
  uip_ip6addr(&client_next, 0xfe80, 0, 0, 0, 0x0201, 0x0001, 0x0001, 0x0001);
  uip_ds6_route_add(&client_ipaddr, 128, &client_next);

  uip_ip6addr(&ipaddr, 0xaaaa, 0, 0, 0, 0, 0, 0, 0);
  uip_ds6_set_addr_iid(&ipaddr, &uip_lladdr);
  uip_ds6_addr_add(&ipaddr, 0, ADDR_MANUAL);
  root_if = uip_ds6_addr_lookup(&ipaddr);
  if(root_if != NULL) {
    rpl_dag_t *dag;
    dag = rpl_set_root(RPL_DEFAULT_INSTANCE, (uip_ip6addr_t *)&ipaddr);
    uip_ip6addr(&ipaddr, 0xaaaa, 0, 0, 0, 0, 0, 0, 0);
    rpl_set_prefix(dag, &ipaddr, 64);
    PRINTF("created a new RPL dag\n");
  } else {
    PRINTF("failed to create a new RPL DAG\n");
  }
#endif /* UIP_CONF_ROUTER */

  print_local_addresses();

  spd_conf_init();
  rpl_sad_init();
  PRINTF("SAD and SPD initialized\n");
  ike_init();
  PRINTF("IKEv2 service initialized\n");

  while(1) {
    PROCESS_WAIT_EVENT();
    if(ev == ike_negotiate_done) {
      /* For some reason Cooja does not get this event sometimes*/
      printf("IKE Negotiation done event Server\n");
      ike_statem_session_t *session = (ike_statem_session_t *)data;
      printf("Session %p\n", session);
      printf("Generated Key: %u\n", session->incoming_entry->sa.encr_keylen);
      hexdump(session->incoming_entry->sa.sk_e, session->incoming_entry->sa.encr_keylen);
      printf("Outgoing SAD entry\n");
      PRINTRPLSADENTRY(session->outgoing_entry);
      printf("Incoming SAD entry\n");
      PRINTRPLSADENTRY(session->incoming_entry);
    }
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
