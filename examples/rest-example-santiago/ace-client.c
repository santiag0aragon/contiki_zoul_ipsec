/*
 * Copyright (c) 2013, Institute for Pervasive Computing, ETH Zurich
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 */

/**
 * \file
 *      Erbium (Er) CoAP client example.
 * \author
 *      Matthias Kovatsch <kovatsch@inf.ethz.ch>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "contiki.h"
#include "contiki-net.h"
#include "er-coap-engine.h"
#include "dev/button-sensor.h"

#include "ace-token.h"

#include "net/ip/uip.h"
#include "sad.h"
#define DEBUG 1
#if DEBUG
#include <stdio.h>
#define PRINTF(...) printf(__VA_ARGS__)
#define PRINT6ADDR(addr) PRINTF("[%02x%02x:%02x%02x:%02x%02x:%02x%02x:" \
                                "%02x%02x:%02x%02x:%02x%02x:%02x%02x]", \
                                ((uint8_t *)addr)[0], ((uint8_t *)addr)[1], \
                                ((uint8_t *)addr)[2], ((uint8_t *)addr)[3], \
                                ((uint8_t *)addr)[4], ((uint8_t *)addr)[5], \
                                ((uint8_t *)addr)[6], ((uint8_t *)addr)[7], \
                                ((uint8_t *)addr)[8], ((uint8_t *)addr)[9], \
                                ((uint8_t *)addr)[10], ((uint8_t *)addr)[11], \
                                ((uint8_t *)addr)[12], ((uint8_t *)addr)[13], \
                                ((uint8_t *)addr)[14], ((uint8_t *)addr)[15])

#define PRINTADDR(addr) PRINTF("[%d.%d.%d.%d]", \
                                ((uint8_t *)addr)[0], ((uint8_t *)addr)[1], \
                                ((uint8_t *)addr)[2], ((uint8_t *)addr)[3])

#define PRINTLLADDR(lladdr) PRINTF("[%02x:%02x:%02x:%02x:%02x:%02x]", \
                                    (lladdr)->addr[0], (lladdr)->addr[1], \
                                    (lladdr)->addr[2], (lladdr)->addr[3], \
                                    (lladdr)->addr[4], (lladdr)->addr[5])
#else
#define PRINTF(...)
#define PRINT6ADDR(addr)
#define PRINTLLADDR(addr)
#define PRINTADDR(addr)
#endif

//  http://[aaaa::c30c:0:0:2]/
//  fe80::20c:29ff:fe74:7b91
// 2001:db8::/32
// #define AUTH_SERVER_NODE(ipaddr) uip_ip6addr(ipaddr, 0x2001, 0xdb8, 0, 0, 0, 0, 0, 0)
// #define RES_SERVER_NODE(ipaddr) uip_ip6addr(ipaddr, 0xfe80, 0, 0, 0, 0xc30c, 0, 0, 3)
// #define RES_SERVER_NODE(ipaddr) uip_ip6addr(ipaddr, 0xfe80, 0, 0, 0, 0x200, 0, 0, 3)
// #define RES_SERVER_NODE(ipaddr) uip_ip6addr(ipaddr, 0xfe80, 0, 0, 0, 0x212, 0x4b00, 0x9df, 0x4f16)
#define RES_SERVER_NODE(ipaddr) uip_ip6addr(ipaddr, 0xaaaa, 0x0, 0x0, 0, 0x212, 0x4b00, 0x9df, 0x4f16)
// #define RES_SERVER_NODE(ipaddr) uip_ip6addr(ipaddr, 0x6374, 0x6eb7, 0x100, 0, 0x212, 0x4b00, 0x9df, 0x8ecb)
//uip_ip6addr(ipaddr, 0, 0, 0, 0, 0, 0, 0, 0)
// uip_ip6addr(ipaddr, 0xaaaa, 0, 0, 0, 0xc30c, 0, 0, 0x2)
// uip_ip6addr(ipaddr, 2001, db8, 0, 0, 0, 0, 0, 0)

#define REMOTE_PORT     UIP_HTONS(COAP_DEFAULT_PORT) // COAP
#define TOGGLE_INTERVAL 10

PROCESS(client, "Client");
AUTOSTART_PROCESSES(&client);

uip_ipaddr_t server_ipaddr;
// static struct etimer et;

/* Example URIs that can be queried. */
#define NUMBER_OF_URLS 2
/* leading and ending slashes only for demo purposes, get cropped automatically when setting the Uri-Path */
char *service_urls[NUMBER_OF_URLS] =
{ ".well-known/core", "token"};
static int uri_switch = 0;
// #endif


#include "net/ip/uip.h"
#include "net/ipv6/uip-ds6.h"
#include "net/ip/uip-udp-packet.h"
static void
print_local_addresses(void)
{
  int i;
  uint8_t state;

  PRINTF("Client IPv6 addresses: ");
  for(i = 0; i < UIP_DS6_ADDR_NB; i++) {
    state = uip_ds6_if.addr_list[i].state;
    if(uip_ds6_if.addr_list[i].isused &&
       (state == ADDR_TENTATIVE || state == ADDR_PREFERRED)) {
      PRINT6ADDR(&uip_ds6_if.addr_list[i].ipaddr);
      PRINTF("\n");
      /* hack to make address "final" */
      if (state == ADDR_TENTATIVE) {
  uip_ds6_if.addr_list[i].state = ADDR_PREFERRED;
      }
    }
  }
}
/* This function is will be passed to COAP_BLOCKING_REQUEST() to handle responses. */
void
client_chunk_handler(void *response)
{
  const uint8_t *chunk;

  int len = coap_get_payload(response, &chunk);

  printf("|%.*s", len, (char *)chunk);
}
PROCESS_THREAD(client, ev, data)
{
  PROCESS_BEGIN();
    SENSORS_ACTIVATE(button_sensor);
    print_local_addresses();
//     static uip_ip6addr_t ip_other;
//     static uip_ip6addr_t ip_me;
//     const uint8_t encr_key[] =
//     // {
//     //   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x64, 0x2b, 0xf6, 0x13, 0xf8, 0xa1, 0x00, 0x00,0x00, 0x00, 0x07, 0x59, 0x0
//     // };
//     //
//     {
//       0x3b, 0xda, 0x5b, 0x6c, 0x05, 0x59, 0x5d, 0xe5, 0x64, 0x2b, 0xf6, 0x13, 0xf8, 0xd1, 0xaf, 0xd4,0xd4, 0xa8, 0x07, 0x59
//     };
//     uip_ip6addr(&ip_me,    0xaaaa, 0, 0, 0x0, 0x212, 0x4b00, 0x9df, 0x904f);
//     uip_ip6addr(&ip_other, 0xaaaa, 0x0, 0x0, 0, 0x212, 0x4b00, 0x9df, 0x4f16);
//     sad_set(
//         &ip_other,
//         &ip_me,
//         &encr_key,
//         sizeof(encr_key),
//         1,
//         2,// 2,
//         SA_PROTO_ESP,//new_ipsec_sa.prot_type, // SA_PROTO_ESP,
//         SA_ENCR_AES_CTR);//new_ipsec_sa.encr_alg);// SA_ENCR_AES_CTR);
// printf("Setting SA. DONE :D\n");
    static coap_packet_t request[1];      /* This way the packet can be treated as pointer as usual. */
    RES_SERVER_NODE(&server_ipaddr);
    coap_init_engine();

    // while(1) {
    // PROCESS_WAIT_EVENT_UNTIL(ev == sensors_event && data == &button_sensor);
      while(uri_switch  < NUMBER_OF_URLS) {
        if (uri_switch == 0){
          printf("--Starting requests --\n");
        }

        printf("\n--Requesting %s--\n", service_urls[uri_switch]);
        if (uri_switch == 1){
          // HERE THE ACCESS TOKEN SHOULD BE INCLUDED IN THE REQUEST
          // TO THEtoken ENDPOINT
          // Assuming that the TOken was issued by AS
          uint8_t payload_bytes[512];
          uint16_t payload_len;
          payload_len = generate_dummy_access_token(payload_bytes);



           // cbor_data cb_data;
           // cb_data.buf =  payload_bytes;
           // cb_data.ptr = 0;

           // ipsec_sa new_ipsec_sa;
           // decode_cbor_web_token(&cb_data, &new_ipsec_sa);
          // enum cose_type ctype = COSE_SIGN1;
          // enum cose_type ctype = COSE_ENCRYPT0;
          //enum cose_type ctype = COSE_MAC0;

          // uint8_t data_bytes[512];
          // cbor_data cb_data;
          // cb_data.buf = payload_bytes;
          // cb_data.ptr = 0;
          // decode_cbor_web_token(&cb_data);
          // wrapin_cose_envelope(&cb_data, ctype, payload_bytes, payload_len);



          ///
          coap_init_message(request, COAP_TYPE_CON, COAP_POST, 0);
          // char const *const access_token = "Access Token: 12345";
          // int at_len = strlen(access_token);

          coap_set_payload(request, &payload_bytes, payload_len);
        }
        else{
          coap_init_message(request, COAP_TYPE_CON, COAP_GET, 0);
        }

        coap_set_header_uri_path(request, service_urls[uri_switch]);
        // PRINT6ADDR(&server_ipaddr);
        // PRINTF(" : %u\n", UIP_HTONS(REMOTE_PORT));
        COAP_BLOCKING_REQUEST(&server_ipaddr, REMOTE_PORT, request,
                              client_chunk_handler);
        // uri_switch = ((uri_switch + 1) % NUMBER_OF_URLS);
        uri_switch = ((uri_switch + 1));
      }

  // }


  PROCESS_END();
  }
