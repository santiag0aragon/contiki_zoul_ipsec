#ifndef COSE_ENCODER_H_
#define COSE_ENCODER_H_

#include "cbor-encoder.h"


#define MAX_COSE_HDR_ENTRY			10
#define MAX_COSE_HDR_VALUE_LEN		20



enum cbor_tag {
	TBD1 = 1,
	TBD2 = 2,
	TBD3 = 3,
	TBD4 = 4,
	TBD6 = 6,
	TBD7 = 7
};

enum cose_type {
	COSE_SIGN     = TBD1,	/* unsigned integer */
	COSE_SIGN1    = TBD7,	/* negative integer */
	COSE_ENCRYPT  = TBD2,	/* byte string */
	COSE_ENCRYPT0 = TBD3,	/* text string */
	COSE_MAC      = TBD4,	/* array of data items */
	COSE_MAC0     = TBD6	/* map of pair of data items */
};


/******   COSE common header parameters *************/
enum cose_common_hdr_param {
	ALG 	 = 1,
	CRIT 	 = 2,
	CON_TYPE = 3,
	KEY_ID   = 4,
	IV 		 = 5,
	PAR_IV   = 6,
	CNT_SIGN = 7
};


typedef struct cose_header_struct {
	uint8_t label[MAX_COSE_HDR_ENTRY];
	uint8_t value[MAX_COSE_HDR_ENTRY][MAX_COSE_HDR_VALUE_LEN];
	int8_t protected;
	uint8_t map_len;
} cose_header;




#define SIGNATURE_LEN			40

#define HEADER_LEN				40

#define MAC_TAG_LEN				40



void initiate_cose_hdr_struct(cose_header *cose_hdr, uint8_t protected);


int8_t add_cose_hdr_param(cose_header *cose_hdr, uint8_t label, uint8_t *value, uint8_t len);


void encode_cose_sign1(cbor_data *data, cose_header *cose_hdr, uint8_t *payload, uint16_t len);									
void encode_cose_encrypt0(cbor_data *data, cose_header *cose_hdr, uint8_t *payload_bytes, uint16_t payload_len);						
void encode_cose_mac0(cbor_data *data, cose_header *cose_hdr, uint8_t *payload, uint16_t len);


void encode_cose_msg(cbor_data *data, cose_header *cose_hdr, enum cose_type ctype, 
						uint8_t *payload_bytes, uint16_t payload_len);

uint8_t decode_cose_msg(cbor_data *data, cose_header *cose_hdr, enum cose_type ctype, 
						uint8_t *payload_bytes);


uint8_t decode_cose_encrypt0(cbor_data *data, cose_header *cose_hdr, uint8_t *payload_bytes);

uint8_t decode_cose_mac0(cbor_data *data, cose_header *cose_hdr, uint8_t *payload_bytes);



#endif  /* COSE_ENCODER_H_ */
