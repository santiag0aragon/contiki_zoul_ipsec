
#include "cbor-encoder.h"
#include "contiki.h"
#include "cbor-web-token.h"
#include "cose-encoder.h"

uint8_t generate_cose_key( uint8_t *payload_bytes,
	                       char *cose_key_type,
	                       char *cose_key_id,
	                       char *key) {

	 cbor_data payload;
	 payload.buf = payload_bytes;
	 payload.ptr = 0;
	 /* encode CBOR web token as a payload */
	 encode_map_array(&payload, 3);

	 encode_cose_key(&payload, COSE_KEY_TYPE, (uint8_t *) cose_key_type);
	 encode_cose_key(&payload, COSE_KEY_ID, (uint8_t *) cose_key_id);
	 encode_cose_key(&payload, KEY, (uint8_t *) key);
	 return payload.ptr;
}

// uint8_t generate_ipsec(uint8_t *payload_bytes,
//                        char *mode,
//                        char *protocol,
//                        uint32_t life,
//                        char *ip_c,
//                        char *ip_rs,
//                        uint32_t spi_sa_c,
//                        uint32_t spi_sa_rs,
//                        char *algorithm,
//                        char *seed) {

// 	 cbor_data payload;
// 	 payload.buf = payload_bytes;
// 	 payload.ptr = 0;
// 	 uint8_t dstr[4];
// 	 /* encode CBOR web token as a payload */
// 	 encode_map_array(&payload, 9);

// 	 encode_ipsec(&payload, MODE, (uint8_t *) mode);
// 	 encode_ipsec(&payload, PROTOCOL, (uint8_t *) protocol);
// 	 integer2bytestring(life, 4, dstr);
// 	 encode_ipsec(&payload, LIFE, dstr);
// 	 encode_ipsec(&payload, IP_C, (uint8_t *) ip_c);
// 	 encode_ipsec(&payload, IP_RS, (uint8_t *)ip_rs);
// 	 integer2bytestring(spi_sa_c, 4, dstr);
// 	 encode_ipsec(&payload, SPI_SA_C, dstr);
// 	 integer2bytestring(spi_sa_rs, 4, dstr);
// 	 encode_ipsec(&payload, SPI_SA_RS, dstr);
// 	 encode_ipsec(&payload, IPSEC_ALG, (uint8_t *) algorithm);
// 	 encode_ipsec(&payload, SEED, (uint8_t *) seed);
// 	 return payload.ptr;
// }

// uint8_t generate_cnf( uint8_t *payload_bytes,
// 	                       char *cose_key,
// 	                       char *cose_encrypted,
// 	                       char *cnf_key_id,
// 	                       char *ipsec,
// 	                       char *kmp) {

// 	 cbor_data payload;
// 	 payload.buf = payload_bytes;
// 	 payload.ptr = 0;
// 	 /* encode CBOR web token as a payload */
// 	 encode_map_array(&payload, 5);

// 	 encode_cnf(&payload, COSE_KEY, (uint8_t *) cose_key);
// 	 encode_cnf(&payload, COSE_ENCRYPTED, (uint8_t *) cose_encrypted);
// 	 encode_cnf(&payload, CNF_KEY_ID, (uint8_t *) cnf_key_id);
// 	 encode_cnf(&payload, IPSEC, (uint8_t *) ipsec);
// 	 encode_cnf(&payload, KMP, (uint8_t *) kmp);
// 	 return payload.ptr;
// }
uint8_t generate_access_token( uint8_t *payload_bytes,
	                           char *subject,
	                           char *audience,
	                           uint32_t exp_time,
	                           char *client_id,
	                           char *scope,
	                           uint32_t grant_type,
	                           char *access_token,
	                           char *token_type,
	                           char *username,
	                           char *password,
	                           char *cnf,
	                           char *profile) {

	 cbor_data payload;
	 payload.buf = payload_bytes;
	 payload.ptr = 0;
	 uint8_t dstr[4];

	 /* encode CBOR web token as a payload */
	 encode_map_array(&payload, 11);

	 encode_cwt_directly(&payload, SUB, (uint8_t *) subject);
	 encode_cwt_directly(&payload, AUD, (uint8_t *) audience);
	 integer2bytestring(exp_time, 4, dstr);
	 encode_cwt_directly(&payload, EXP, dstr);
	 encode_cwt_directly(&payload, CLIENT_ID, (uint8_t *) client_id);
	 encode_cwt_directly(&payload, SCOPE, (uint8_t *) scope);
	 integer2bytestring(grant_type, 4, dstr);
	 encode_cwt_directly(&payload, GRANT_TYPE, dstr); // password
	 encode_cwt_directly(&payload, ACCESS_TOKEN, (uint8_t *) access_token);
	 encode_cwt_directly(&payload, TOKEN_TYPE, (uint8_t *) token_type);
	 encode_cwt_directly(&payload, USERNAME, (uint8_t *) username);
	 encode_cwt_directly(&payload, PASSWORD, (uint8_t *) password);

	// uint8_t cose_key_bytes[256];
	// uint16_t cose_key_len;
 //    cose_key_len = generate_cose_key( cose_key_bytes,
	// 	                      "Symmetric",
	// 	                      "919191",
	// 	                      "aa280649d445");
 //     printf("\ncose_key len %d\n", cose_key_len);

 // 	uint8_t ipsec_bytes[256];
	// uint16_t ipsec_len;
 //    ipsec_len = generate_ipsec( ipsec_bytes,
	//                        "transport",
	//                        "ESP",
	//                        12345,
	//                        "a.b.c.d1",
	//                        "a.b.c.d2",
	//                        life,
	//                        life,
	//                        "SA_ENCR_AES_CTR",
	//                        "aa280649d445");
 //     printf("\nipsec len %d\n", ipsec_len);

	// uint8_t cnf_bytes[256];
	// uint16_t cnf_len;
 //    cnf_len = generate_cnf(cnf_bytes,
 //                        cose_key_bytes,
 //                        "132789132hasd",
 //                        "1001",
 //                        ipsec_bytes,
 //                      	"ikv2");
     // printf("\ncnf len %d\n", cnf_len);
	 // encode_cwt_directly(&payload, CNF, (uint8_t *) cnf_bytes);
	 encode_cwt_directly(&payload, PROFILE, (uint8_t *) profile);
	 return payload.ptr;
}

uint8_t generate_oauth_as_cwt_payload(uint8_t *payload_bytes) {
 	uint32_t time = 3600;
 	uint32_t grant_type = 10;

	return generate_access_token(payload_bytes,
								 "subject",
								 "coap://example.com",
								 time,
								 "a_client_id",
								 "read_scope",
								 grant_type,
								 "an_encoded_access_token",
								 "P-o-P",
								 "client_username",
								 "client_password",
								 "a_cnf",
								 "coap_ipsec");
	// cbor_data payload;
	// payload.buf = payload_bytes;
	// payload.ptr = 0;

	// /* encode CBOR web token as a payload */
	// encode_map_array(&payload, 11);

	// encode_cwt_directly(&payload, SUB, (uint8_t *) "subject");
	// encode_cwt_directly(&payload, AUD, (uint8_t *) "coap://example.com");
	// uint8_t dstr[4];
	// uint32_t time;
	// time = 1479132451;
	// integer2bytestring(time, 4, dstr);

	// encode_cwt_directly(&payload, EXP, dstr);
	// encode_cwt_directly(&payload, CLIENT_ID, (uint8_t *) "a_client_id");
	// encode_cwt_directly(&payload, SCOPE, (uint8_t *) "read");
	// encode_cwt_directly(&payload, GRANT_TYPE, 0); // password
	// encode_cwt_directly(&payload, ACCESS_TOKEN, (uint8_t *) "an_encoded_access_token");
	// encode_cwt_directly(&payload, TOKEN_TYPE, (uint8_t *) "P-o-P");
	// encode_cwt_directly(&payload, USERNAME, (uint8_t *) "client_username");
	// encode_cwt_directly(&payload, PASSWORD, (uint8_t *) "client_password");
	// // encode_cwt_directly(&payload, CNF, (uint8_t *) "coap://example.com");
	// encode_cwt_directly(&payload, PROFILE, (uint8_t *) "coap_ipsec");

	// return payload.ptr;
}





PROCESS(cbor_test, "cbor test");
AUTOSTART_PROCESSES(&cbor_test);
PROCESS_THREAD(cbor_test, ev, data)
{
	PROCESS_BEGIN();

	/************** at the OAuth Server *****************/
	uint8_t payload_bytes[512];
	uint16_t payload_len;
	payload_len = generate_oauth_as_cwt_payload(payload_bytes);

	enum cose_type ctype = COSE_SIGN1;
	// enum cose_type ctype = COSE_ENCRYPT0;
	//enum cose_type ctype = COSE_MAC0;

	uint8_t data_bytes[512];
	cbor_data cb_data;
	cb_data.buf = data_bytes;
	cb_data.ptr = 0;
	wrapin_cose_envelope(&cb_data, ctype, payload_bytes, payload_len);



	/*********** IoT Server received from client *****************/

	cb_data.buf = data_bytes;
	cb_data.ptr = 0;
	payload_len = unwrap_cose_envelope(&cb_data, ctype, payload_bytes, payload_len);
	printf("payload len %d\n", payload_len);
	if(payload_len) {
		cb_data.buf = payload_bytes;
		cb_data.ptr = 0;
		decode_cbor_web_token_no_ipsec(&cb_data, NULL);
		// decode_ipsec(&data);
		// decode_cnf(&data);
	}
	PROCESS_END();
}


/** @} */
/** @} */
/** @} */
