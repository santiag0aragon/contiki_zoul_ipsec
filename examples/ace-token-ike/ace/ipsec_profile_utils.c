
// #include "cbor-encoder.h"
// #include "cbor-web-token.h"
// #include "cose-encoder.h"
#include "ipsec_profile_utils.h"
#include "contiki.h"
#include "ace-token.h"
#include "sad.h"
#include "ike/auth.h"
#include "spd.h"
// #include "cbor-web-token.h"

void process_direct_provisioning_token_rs( ipsec_sa new_ipsec_sa){
  static uip_ip6addr_t ip_other;
  uip_ip6addr(&ip_other,    0xfe80, 0, 0, 0x0, 0x212, 0x4b00, 0x9df, 0x904f);
  const uint8_t encr_key[] =
  {
    0x3b, 0xda, 0x5b, 0x6c, 0x05, 0x59, 0x5d, 0xe5, 0x64, 0x2b, 0xf6, 0x13, 0xf8, 0xd1, 0xaf, 0xd4,0xd4, 0xa8, 0x07, 0x59
  };
    printf("Initializing SPD...\n");
    printf("Setting SA...\n");
    sad_set(
        &ip_other,
        &encr_key,
        sizeof(encr_key),
        new_ipsec_sa.spi_sa_rs,
        new_ipsec_sa.spi_sa_c,// 2,
        new_ipsec_sa.prot_type,//new_ipsec_sa.prot_type, // SA_PROTO_ESP,
        new_ipsec_sa.encr_alg);//new_ipsec_sa.encr_alg);// SA_ENCR_AES_CTR);
    printf("Setting SA. DONE :D\n");
}
void process_direct_provisioning_token_client( ipsec_sa new_ipsec_sa){

  static uip_ip6addr_t ip_other;
  uip_ip6addr(&ip_other,0xfe80, 0x0, 0x0, 0x0, 0x212, 0x4b00, 0x9df, 0x4f16);
  const uint8_t encr_key[] =
  {
  0x3b, 0xda, 0x5b, 0x6c, 0x05, 0x59, 0x5d, 0xe5, 0x64, 0x2b, 0xf6, 0x13, 0xf8, 0xd1, 0xaf, 0xd4,0xd4, 0xa8, 0x07, 0x59
  };
  printf("\n\nSetting up SA\n\n");
  sad_set(
      &ip_other,
      &encr_key,
      sizeof(encr_key),
      new_ipsec_sa.spi_sa_c,// 2,
      new_ipsec_sa.spi_sa_rs,
      new_ipsec_sa.prot_type,//new_ipsec_sa.prot_type, // SA_PROTO_ESP,
      new_ipsec_sa.encr_alg);//new_ipsec_sa.encr_alg);// SA_ENCR_AES_CTR);
  printf("Setting SA. DONE :D\n");
}

void process_psk_token_client(ipsec_sa new_ipsec_sa){
 // uint8_t psk[32] = "2123456789abcdef0123456789abcdef";
  printf("\n\nProcessing token Client side PSK\n\n");

  set_psk(new_ipsec_sa.psk);
  set_spd_rs_null();
};

void process_psk_token_rs(ipsec_sa new_ipsec_sa){
 uint8_t psk[32] = "2123456789abcdef0123456789abcdef";
  printf("\n\nProcessing  token RS side PSK\n\n");
  // set_psk(psk);
  set_spd_client_null();
};
