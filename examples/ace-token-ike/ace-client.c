/*
 * Copyright (c) 2013, Institute for Pervasive Computing, ETH Zurich
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 */

/**
 * \file
 *      Erbium (Er) CoAP client example.
 * \author
 *      Matthias Kovatsch <kovatsch@inf.ethz.ch>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "contiki.h"
#include "contiki-net.h"
#include "er-coap-engine.h"
#include "dev/button-sensor.h"
#include "net/rpl/rpl.h"

#include "ace-token.h"
#include "ipsec_profile_utils.h"
#include "sys/etimer.h"
#include "net/ip/uip.h"
#include "sad.h"

#define DEBUG 1
#if DEBUG
#include <stdio.h>
#define PRINTF(...) printf(__VA_ARGS__)
#define PRINT6ADDR(addr) PRINTF("[%02x%02x:%02x%02x:%02x%02x:%02x%02x:" \
                                "%02x%02x:%02x%02x:%02x%02x:%02x%02x]", \
                                ((uint8_t *)addr)[0], ((uint8_t *)addr)[1], \
                                ((uint8_t *)addr)[2], ((uint8_t *)addr)[3], \
                                ((uint8_t *)addr)[4], ((uint8_t *)addr)[5], \
                                ((uint8_t *)addr)[6], ((uint8_t *)addr)[7], \
                                ((uint8_t *)addr)[8], ((uint8_t *)addr)[9], \
                                ((uint8_t *)addr)[10], ((uint8_t *)addr)[11], \
                                ((uint8_t *)addr)[12], ((uint8_t *)addr)[13], \
                                ((uint8_t *)addr)[14], ((uint8_t *)addr)[15])

#define PRINTADDR(addr) PRINTF("[%d.%d.%d.%d]", \
                                ((uint8_t *)addr)[0], ((uint8_t *)addr)[1], \
                                ((uint8_t *)addr)[2], ((uint8_t *)addr)[3])

#define PRINTLLADDR(lladdr) PRINTF("[%02x:%02x:%02x:%02x:%02x:%02x]", \
                                    (lladdr)->addr[0], (lladdr)->addr[1], \
                                    (lladdr)->addr[2], (lladdr)->addr[3], \
                                    (lladdr)->addr[4], (lladdr)->addr[5])
#else
#define PRINTF(...)
#define PRINT6ADDR(addr)
#define PRINTLLADDR(addr)
#define PRINTADDR(addr)
#endif

//  http://[aaaa::c30c:0:0:2]/
//  fe80::20c:29ff:fe74:7b91
// 2001:db8::/32
#define AUTH_SERVER_NODE(ipaddr) uip_ip6addr(ipaddr, 0xfe80, 0x0, 0x0, 0x0, 0x212, 0x4b00, 0x9df, 0x8ecb)
// #define RES_SERVER_NODE(ipaddr) uip_ip6addr(ipaddr, 0xfe80, 0, 0, 0, 0xc30c, 0, 0, 3)
// #define RES_SERVER_NODE(ipaddr) uip_ip6addr(ipaddr, 0xfe80, 0, 0, 0, 0x200, 0, 0, 3)
// #define RES_SERVER_NODE(ipaddr) uip_ip6addr(ipaddr, 0xfe80, 0, 0, 0, 0x212, 0x4b00, 0x9df, 0x4f16)
#define RES_SERVER_NODE(ipaddr) uip_ip6addr(ipaddr, 0xfe80, 0x0, 0x0, 0x0, 0x212, 0x4b00, 0x9df, 0x4f16)
// #define RES_SERVER_NODE(ipaddr) uip_ip6addr(ipaddr, 0x6374, 0x6eb7, 0x100, 0, 0x212, 0x4b00, 0x9df, 0x8ecb)
//uip_ip6addr(ipaddr, 0, 0, 0, 0, 0, 0, 0, 0)
// uip_ip6addr(ipaddr, 0xaaaa, 0, 0, 0, 0xc30c, 0, 0, 0x2)
// uip_ip6addr(ipaddr, 2001, db8, 0, 0, 0, 0, 0, 0)

#define REMOTE_PORT     UIP_HTONS(COAP_DEFAULT_PORT) // COAP
#define TOGGLE_INTERVAL 10

PROCESS(client, "Client");
AUTOSTART_PROCESSES(&client);

// const static uip_ipaddr_t rs_ipaddr = {
//                                        UIP_HTONS(0xfe80),
//                                         UIP_HTONS(0x0),
//                                         UIP_HTONS(0x0),
//                                         UIP_HTONS(0x0),
//                                         UIP_HTONS(0x212),
//                                         UIP_HTONS(0x4b00),
//                                         UIP_HTONS(0x9df),
//                                         UIP_HTONS(0x4f16),0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,

//                                       };
// const static uip_ipaddr_t as_ipaddr = {{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
//                                        UIP_HTONS(0xfe80),
//                                         UIP_HTONS(0x0),
//                                         UIP_HTONS(0x0),
//                                         UIP_HTONS(0x0),
//                                         UIP_HTONS(0x212),
//                                         UIP_HTONS(0x4b00),
//                                         UIP_HTONS(0x9df),
//                                         UIP_HTONS(0x4f16)
//                                       }
//                                       };
uip_ipaddr_t as_ipaddr;
uip_ipaddr_t rs_ipaddr;
// static struct etimer et;

/* Example URIs that can be queried. */
#define NUMBER_OF_URLS 7
/* leading and ending slashes only for demo purposes, get cropped automatically when setting the Uri-Path */
char *service_urls[NUMBER_OF_URLS] =
// { ".well-known/core", "authz-info", ".well-known/core"};
{".well-known/core", "token", ".well-known/core", "authz-info", "hello","hello", ".well-known/core"};
static int uri_switch = 0;
// #endif
ipsec_sa new_ipsec_sa;
uint8_t payload_bytes[512];
uint16_t payload_len;

#include "net/ip/uip.h"
#include "net/ipv6/uip-ds6.h"
#include "net/ip/uip-udp-packet.h"


static void
print_local_addresses(void)
{
  int i;
  uint8_t state;

  PRINTF("AS IPv6 addresses: ");
  for(i = 0; i < UIP_DS6_ADDR_NB; i++) {
    state = uip_ds6_if.addr_list[i].state;
    if(uip_ds6_if.addr_list[i].isused &&
       (state == ADDR_TENTATIVE || state == ADDR_PREFERRED)) {
      PRINT6ADDR(&uip_ds6_if.addr_list[i].ipaddr);
      PRINTF("\n");
      /* hack to make address "final" */
      if (state == ADDR_TENTATIVE) {
  uip_ds6_if.addr_list[i].state = ADDR_PREFERRED;
      }
    }
  }
}

#if UIP_CONF_ROUTER
static uip_ds6_maddr_t *
set_global_address(void)
{
  uip_ipaddr_t ipaddr;
  uip_ds6_maddr_t *rv;
  uip_ip6addr(&ipaddr, 0xaaaa, 0, 0, 0, 0, 0, 0, 0);
  uip_ds6_set_addr_iid(&ipaddr, &uip_lladdr);
  uip_ds6_addr_add(&ipaddr, 0, ADDR_AUTOCONF);

}
#endif /* UIP_CONF_ROUTER */
/* This function is will be passed to COAP_BLOCKING_REQUEST() to handle responses. */
void
decode_token(void *response)
{
  const uint8_t *chunk;
  int len = coap_get_payload(response, &chunk);

  printf("ACCESS TOKEN RECEIVED\n");

  cbor_data cb_data;
  cb_data.buf =  chunk;
  cb_data.ptr = 0;


  decode_cbor_web_token(&cb_data, &new_ipsec_sa);

  payload_len = len;

  memcpy(&payload_bytes, chunk, len);
}

void process_token_client_handler(){
  #if WITH_CONF_MANUAL_SA
    process_direct_provisioning_token_client(new_ipsec_sa);
  #endif

  #if WITH_CONF_IPSEC_IKE
    process_psk_token_client(new_ipsec_sa);
  #endif
}

void
client_chunk_handler1(void *response)
{
  const uint8_t *chunk;
  int len = coap_get_payload(response, &chunk);
  printf("\n\n|%.*s\n\n", len, (char *)chunk);
}



PROCESS_THREAD(client, ev, data){
  static struct etimer et;
  PROCESS_BEGIN();

  etimer_set(&et, CLOCK_SECOND * 5);

  set_global_address();
  PRINTF("Starting Client\n");
  print_local_addresses();
  PRINTF("uIP buffer: %u\n", UIP_BUFSIZE);
  PRINTF("LL header: %u\n", UIP_LLH_LEN);
  PRINTF("IP+UDP header: %u\n", UIP_IPUDPH_LEN);
  PRINTF("REST max chunk: %u\n", REST_MAX_CHUNK_SIZE);


    static coap_packet_t request[1];      /* This way the packet can be treated as pointer as usual. */
    AUTH_SERVER_NODE(&as_ipaddr);
    RES_SERVER_NODE(&rs_ipaddr);
    coap_init_engine();


    // while(1) {
    // PROCESS_WAIT_EVENT_UNTIL(ev == sensors_event && data == &button_sensor);


  // static struct uip_udp_conn *client_conn;
  // client_conn = udp_new(&as_ipaddr, UIP_HTONS(1234), NULL);
  // udp_bind(client_conn, UIP_HTONS(3001));
  // static char buf[2];
  while(1){
  PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));
  etimer_reset(&et);
  // sprintf(buf, "H");

  // // sprintf(buf, "Hello %d from the client", ++seq_id);
  // printf(" (msg: %s)\n", buf);
  // uip_udp_packet_send(client_conn, buf, strlen(buf));
  // uip_udp_packet_send(client_conn, buf, UIP_APPDATA_SIZE);

      while(uri_switch  < NUMBER_OF_URLS) {
        printf("\n--Requesting %s to: ", service_urls[uri_switch]);
        if (uri_switch == 0){
          printf("--AS--\n");
          printf("-------------------------\n");
          printf("REQUESTING INFO FROM AS\n");
          PRINT6ADDR(&as_ipaddr);
          printf("\n-------------------------\n");
          coap_init_message(request, COAP_TYPE_CON, COAP_GET, coap_get_mid());
          coap_set_header_uri_path(request, service_urls[uri_switch]);
          COAP_BLOCKING_REQUEST(&as_ipaddr, REMOTE_PORT, request,
                              client_chunk_handler1);
        } else if (uri_switch == 1){ // REQUEST TOKEN to AS
          printf("--AS--\n");
          PRINT6ADDR(&as_ipaddr);
          printf("-------------------------\n");
          printf("REQUESTING TOKEN FROM AS\n");
          printf("\n-------------------------\n");
          coap_init_message(request, COAP_TYPE_CON, COAP_GET, coap_get_mid());
          coap_set_header_uri_path(request, service_urls[uri_switch]);
          COAP_BLOCKING_REQUEST(&as_ipaddr, REMOTE_PORT, request,
                              decode_token);
        printf("---------------------------\n");
        } else if (uri_switch == 2){ //
          printf("--RS--\n");
          printf("-------------------------\n");
          printf("REQUESTING UNPROTECTED RESOURCE\n");
          RES_SERVER_NODE(&rs_ipaddr); // there is a Buffer overflow after receiveing the token
          PRINT6ADDR(&rs_ipaddr);
          printf("\n-------------------------\n");
          coap_init_message(request, COAP_TYPE_CON, COAP_GET, coap_get_mid());
          coap_set_header_uri_path(request, service_urls[uri_switch]);
          COAP_BLOCKING_REQUEST(&rs_ipaddr, REMOTE_PORT, request,
                              client_chunk_handler1);
        } else if (uri_switch == 4){ //
          printf("--RS--\n");
          printf("-------------------------\n");
          printf("REQUESTING FIRST PROTECTED RESOURCE\n");
          PRINT6ADDR(&rs_ipaddr);
          printf("\n-------------------------\n");
          // set_psk(new_ipsec_sa.psk);
          coap_init_message(request, COAP_TYPE_CON, COAP_GET, coap_get_mid());
          coap_set_header_uri_path(request, service_urls[uri_switch]);
          COAP_BLOCKING_REQUEST(&rs_ipaddr, REMOTE_PORT, request,
                              client_chunk_handler1);
} else if (uri_switch > 4){ //
          printf("--RS--\n");
          printf("-------------------------\n");
          printf("REQUESTING PROTECTED RESOURCE\n");
          PRINT6ADDR(&rs_ipaddr);
          printf("\n-------------------------\n");
          coap_init_message(request, COAP_TYPE_CON, COAP_GET, coap_get_mid());
          coap_set_header_uri_path(request, service_urls[uri_switch]);
          COAP_BLOCKING_REQUEST(&rs_ipaddr, REMOTE_PORT, request,
                              client_chunk_handler1);
        } else if (uri_switch == 3){ // POST TOKEN to RS
          printf("--RS--\n");
          printf("-------------------------\n");
          printf("POSTING TOKEN TO RS\n");
          PRINT6ADDR(&rs_ipaddr);
          printf("\n-------------------------\n");
          coap_init_message(request, COAP_TYPE_CON, COAP_POST, coap_get_mid());
          coap_set_payload(request, &payload_bytes, payload_len);
          coap_set_header_uri_path(request, service_urls[uri_switch]);
          COAP_BLOCKING_REQUEST(&rs_ipaddr, REMOTE_PORT, request,
                              client_chunk_handler1);
          #if WITH_CONF_MANUAL_SA
            process_direct_provisioning_token_client(new_ipsec_sa);
          #endif

          #if WITH_CONF_IPSEC_IKE
            process_psk_token_client(new_ipsec_sa);
          #endif
// COAP_BLOCKING_REQUEST_DO(&rs_ipaddr, REMOTE_PORT, request,
//                               client_chunk_handler1,process_token_client_handler);

          // #if WITH_CONF_IPSEC_IKE
          //     set_spd_rs_null();
          // #endif
        }



        // uri_switch = ((uri_switch + 1) % NUMBER_OF_URLS);
        uri_switch = ((uri_switch + 1));
      }
    }

  // }


  PROCESS_END();
  }
