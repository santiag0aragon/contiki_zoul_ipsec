#ifndef __IPSEC_PROJECT_H__
#define __IPSEC_PROJECT_H__

/*

/* IP buffer size must match all other hops, in particular the border router. */


#ifndef LPM_CONF_ENABLE
#define LPM_CONF_ENABLE       1 /**< Set to 0 to disable LPM entirely */
#endif

#ifndef LPM_CONF_MAX_PM
#define LPM_CONF_MAX_PM       1 /* was 1 */
#endif

#ifndef LPM_CONF_STATS
#define LPM_CONF_STATS        1 /**< Set to 1 to enable LPM-related stats */
#endif

/* Disabling RDC and CSMA for demo purposes. Core updates often
   require more memory. */
/* For projects, optimize memory and enable RDC and CSMA again. */

#undef NETSTACK_CONF_RDC
#define NETSTACK_CONF_RDC     nullrdc_driver

#undef NETSTACK_CONF_MAC
#define NETSTACK_CONF_MAC     nullmac_driver

/* Turn off DAO ACK to make code smaller */
#undef RPL_CONF_WITH_DAO_ACK
#define RPL_CONF_WITH_DAO_ACK          0

#undef RPL_CONF_MAX_DAG_PER_INSTANCE
#define RPL_CONF_MAX_DAG_PER_INSTANCE     1

/* Disabling TCP on CoAP nodes. */
#undef UIP_CONF_TCP
#define UIP_CONF_TCP                   0

#undef UIP_CONF_BUFFER_SIZE
#define UIP_CONF_BUFFER_SIZE           1280

/* Increase rpl-border-router IP-buffer when using more than 64. */
#undef REST_MAX_CHUNK_SIZE
#define REST_MAX_CHUNK_SIZE            700

/* Estimate your header size, especially when using Proxy-Uri. */
/*
   #undef COAP_MAX_HEADER_SIZE
   #define COAP_MAX_HEADER_SIZE           70
 */

/* Multiplies with chunk size, be aware of memory constraints. */
#undef COAP_MAX_OPEN_TRANSACTIONS
#define COAP_MAX_OPEN_TRANSACTIONS     4

/* Must be <= open transactions, default is COAP_MAX_OPEN_TRANSACTIONS-1. */
/*
   #undef COAP_MAX_OBSERVERS
   #define COAP_MAX_OBSERVERS             2
 */

/* Filtering .well-known/core per query can be disabled to save space. */
#undef COAP_LINK_FORMAT_FILTERING
#define COAP_LINK_FORMAT_FILTERING     0
#undef COAP_PROXY_OPTION_PROCESSING
#define COAP_PROXY_OPTION_PROCESSING   0

/* Turn off DAO ACK to make code smaller */
#undef RPL_CONF_WITH_DAO_ACK
#define RPL_CONF_WITH_DAO_ACK          0

/* Enable client-side support for COAP observe */
#define COAP_OBSERVE_CLIENT 1

#define UIP_CONF_ROUTER 1
#define WATCHDOG_CONF_ENABLE 0

// #define WITH_IPSEC_SICSLOWPAN   1

#undef DEBUG
#define DEBUG 1
#define IPSEC_DEBUG 1
#define IKE_IPSEC_INFO 1
#define IPSEC_SAD_DBG 1


#define CONTIKI_WITH_IPV6       1
#define NETSTACK_CONF_WITH_IPV6 1

/* IPsec configuration ------------------*/
/* Enabling ESP is equal to enabling to IPsec. Note that AH is not supported! */
// #define WITH_CONF_IPSEC_ESP   1
// #define WITH_CONF_IPSEC_IKE   1
// #define WITH_CONF_MANUAL_SA   0



#define IKE_SAD_ENTRIES 5
#define IKE_SESSION_NUM 5
// #define WITH_CONF_MANUAL_SA   1
/*
 * Enabling certificate authentication for the IKE AUTH payload
 * If the responder uses certificate authentication
 */

/* Shared secret to use when not using certificate authentication */
/* Shared secret defined below used in the IKE_AUTH payload */
#define SHARED_IKE_SECRET "aa280649dc17aa821ac305b5eb09d445"

/* ID up to 16 bytes*/
#define IKE_ID "test@sics.se"

/* IKE SA Configuration */
/* Integrity transform to use for the IKE SA, supported transforms:
   SA_INTEG_AES_XCBC_MAC_96, SA_INTEG_HMAC_SHA1_96, SA_INTEG_HMAC_SHA2_256_128
   NOTE: don't define if IKE_ENCR = SA_ENCR_CCM_{8,12,16}*/
//#define IKE_INTEG SA_INTEG_HMAC_SHA2_256_128

/* Encryption transform to use for the IKE SA, supported transforms:
   SA_ENCR_AES_CTR, SA_ENCR_AES_CCM_8, SA_ENCR_AES_CCM_12, SA_ENCR_AES_CCM_16 */
#define IKE_ENCR SA_ENCR_AES_CCM_8

/* Psuedo-random function to use for the IKE SA, supported functions:
   SA_PRF_HMAC_SHA1, SA_PRF_HMAC_SHA2_256*/
#define IKE_PRF SA_PRF_HMAC_SHA2_256

/* Diffie-Hellman groups supported for IKE:
   SA_DH_192_RND_ECP_GROUP, SA_DH_256_RND_ECP_GROUP, SA_DH_256_BP_GROUP
   NOTE: When certificate authentication is used this defaults to
   SA_DH_256_RND_ECP_GROUP also this parameter must match CURVE_PARAMS from
   the makefile, that is
   SA_DH_192_RND_ECP_GROUP => CURVE_PARAMS = SECP192R1
   SA_DH_256_RND_ECP_GROUP => CURVE_PARAMS = SECP256R1 (Only supported ECDSA algorithm)
   SA_DH_256_BP_GROUP      => CURVE_PARAMS = BPOOLP256R1
 */
#define IKE_DH SA_DH_256_RND_ECP_GROUP

/* ESP SA Configuration */
/* Supported transforms, same as for IKE SA*/
#define ESP_ENCR  SA_ENCR_AES_CCM_8


#ifdef HW_AES
#define CRYPTO_CONF_AES cc2538_aes
#else
#define CRYPTO_CONF_AES contiki_aes
#endif
/* ECC */
#define WORDS_32_BITS 1
// #define STATIC_ECC_KEY
#define CC2538_RF_CONF_CHANNEL 18
#define QUEUEBUF_CONF_NUM 12


#endif /* __PROJECT_ERBIUM_CONF_H__ */

