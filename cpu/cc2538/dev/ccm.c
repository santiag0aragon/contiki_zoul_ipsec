/*
 * Original file:
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/
 * All rights reserved.
 *
 * Port to Contiki:
 * Copyright (c) 2013, ADVANSEE - http://www.advansee.com/
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/**
 * \addtogroup cc2538-ccm
 * @{
 *
 * \file
 * Implementation of the cc2538 AES-CCM driver
 */
#include "contiki.h"
#include "sys/cc.h"
#include "dev/rom-util.h"
#include "dev/ccm.h"
#include "dev/aes.h"
#include "dev/nvic.h"
#include <stdbool.h>
#include <stdint.h>
/*---------------------------------------------------------------------------*/
static uint8_t
ccm_auth_crypt_start(uint8_t encrypt, uint8_t len_len, uint8_t key_area,
                     const void *nonce, const void *adata, uint16_t adata_len,
                     const void *data_in, void *data_out, uint16_t data_len,
                     uint8_t mic_len, struct process *process)
{
  uint32_t ctrl;
  uint32_t iv[AES_IV_LEN / sizeof(uint32_t)];

  /* Program AES-CCM authentication/crypto operation */
  ctrl = AES_AES_CTRL_SAVE_CONTEXT |                         /* Save context */
    (((MAX(mic_len, 2) - 2) >> 1) << AES_AES_CTRL_CCM_M_S) | /* M */
    ((len_len - 1) << AES_AES_CTRL_CCM_L_S) |                /* L */
    AES_AES_CTRL_CCM |                                       /* CCM */
    AES_AES_CTRL_CTR_WIDTH_128 |                             /* CTR width 128 */
    AES_AES_CTRL_CTR |                                       /* CTR */
    (encrypt ? AES_AES_CTRL_DIRECTION_ENCRYPT : 0);          /* En/decryption */

  /* Prepare the crypto initialization vector
   * Flags: L' = L - 1 */
  ((uint8_t *)iv)[0] = len_len - 1;
  /* Nonce */
  rom_util_memcpy(&((uint8_t *)iv)[CCM_FLAGS_LEN], nonce,
                  CCM_NONCE_LEN_LEN - len_len);
  /* Initialize counter to 0 */
  rom_util_memset(&((uint8_t *)iv)[AES_IV_LEN - len_len], 0, len_len);

  return aes_auth_crypt_start(ctrl, key_area, iv, adata, adata_len,
                              data_in, data_out, data_len, process);
}
/*---------------------------------------------------------------------------*/
static uint8_t
ccm_auth_crypt_get_result(const void *cdata, uint16_t cdata_len,
                          void *mic, uint8_t mic_len)
{
  uint32_t tag[AES_TAG_LEN / sizeof(uint32_t)];
  // printf("\ntag len %d\n",AES_TAG_LEN / sizeof(uint32_t));
  // printf("\nmic len %d\n",mic_len);
  uint16_t data_len;
  uint8_t ret;

  ret = aes_auth_crypt_get_result(NULL, tag);
  // hexdump((uint8_t *)tag, 4);
  // hexdump((uint8_t *)tag, 8);
  // printf("\n\n\n--- RET CODE %d ---\n\n",ret);
  if(ret != CRYPTO_SUCCESS) {
    // printf("\TAG FAIL\n");
    return ret;
  }

  if(cdata != NULL) {
    /* Check MIC */
    data_len = cdata_len - mic_len;
    // printf("\n\n\n---data len %d ---\n\n",data_len);
    // hexdump( &((const uint8_t *)cdata)[data_len], mic_len);
    // hexdump((uint8_t *)tag, mic_len);
    if(rom_util_memcmp(tag, &((const uint8_t *)cdata)[data_len], mic_len)) {
      // printf("\n\n\n---FAIL comparing tag with icv ---\n\n",data_len);
      ret = AES_AUTHENTICATION_FAILED;
    }
  }

  /* Copy tag to MIC */
  rom_util_memcpy(mic, tag, mic_len);

  return ret;
}
/*---------------------------------------------------------------------------*/
uint8_t
ccm_auth_encrypt_start(uint8_t len_len, uint8_t key_area, const void *nonce,
                       const void *adata, uint16_t adata_len, const void *pdata,
                       uint16_t pdata_len, void *cdata, uint8_t mic_len,
                       struct process *process)
{
  return ccm_auth_crypt_start(true, len_len, key_area, nonce, adata, adata_len,
                              pdata, cdata, pdata_len, mic_len, process);
}
/*---------------------------------------------------------------------------*/
uint8_t
ccm_auth_encrypt_get_result(void *mic, uint8_t mic_len)
{
  return ccm_auth_crypt_get_result(NULL, 0, mic, mic_len);
}
/*---------------------------------------------------------------------------*/
uint8_t
ccm_auth_decrypt_start(uint8_t len_len, uint8_t key_area, const void *nonce,
                       const void *adata, uint16_t adata_len, const void *cdata,
                       uint16_t cdata_len, void *pdata, uint8_t mic_len,
                       struct process *process)
{
  uint16_t data_len = cdata_len - mic_len;
// cm_auth_crypt_start(uint8_t encrypt, uint8_t len_len, uint8_t key_area,
//                      const void *nonce, const void *adata, uint16_t adata_len,
//                      const void *data_in, void *data_out, uint16_t data_len,
//                      uint8_t mic_len, struct process *process)
  return ccm_auth_crypt_start(false, len_len, key_area, nonce, adata, adata_len,
                              cdata, pdata, data_len, mic_len, process);
}
/*---------------------------------------------------------------------------*/
uint8_t
ccm_auth_decrypt_get_result(const void *cdata, uint16_t cdata_len,
                            void *mic, uint8_t mic_len)
// {
//   uint32_t aes_ctrl_int_stat;
//   uint16_t pdata_len = cdata_len - mic_len;
//   uint32_t tag[4];

//   aes_ctrl_int_stat = REG(AES_CTRL_INT_STAT);
//   /* Clear the error bits */
//   REG(AES_CTRL_INT_CLR) = AES_CTRL_INT_CLR_DMA_BUS_ERR |
//                           AES_CTRL_INT_CLR_KEY_ST_WR_ERR |
//                           AES_CTRL_INT_CLR_KEY_ST_RD_ERR;

//   NVIC_DisableIRQ(AES_IRQn);
//   crypto_register_process_notification(NULL);

//   /* Disable the master control / DMA clock */
//   REG(AES_CTRL_ALG_SEL) = 0x00000000;

//   if(aes_ctrl_int_stat & AES_CTRL_INT_STAT_DMA_BUS_ERR) {
//     return CRYPTO_DMA_BUS_ERROR;
//   }
//   if(aes_ctrl_int_stat & AES_CTRL_INT_STAT_KEY_ST_WR_ERR) {
//     return AES_KEYSTORE_WRITE_ERROR;
//   }
//   if(aes_ctrl_int_stat & AES_CTRL_INT_STAT_KEY_ST_RD_ERR) {
//     return AES_KEYSTORE_READ_ERROR;
//   }

//   /* Read tag
//    * Wait for the context ready bit */
//   while(!(REG(AES_AES_CTRL) & AES_AES_CTRL_SAVED_CONTEXT_READY));

//   /* Read the tag registers */
//   tag[0] = REG(AES_AES_TAG_OUT_0);
//   tag[1] = REG(AES_AES_TAG_OUT_1);
//   tag[2] = REG(AES_AES_TAG_OUT_2);
//   tag[3] = REG(AES_AES_TAG_OUT_3);

//   /* Clear the interrupt status */
//   REG(AES_CTRL_INT_CLR) = AES_CTRL_INT_CLR_DMA_IN_DONE |
//                           AES_CTRL_INT_CLR_RESULT_AV;

//   /* Check MIC */
//   if(memcmp(tag, &((const uint8_t *)cdata)[pdata_len], mic_len)) {
//     return AES_AUTHENTICATION_FAILED;
//   }

//   /* Copy tag to MIC */
//   memcpy(mic, tag, mic_len);

//   return CRYPTO_SUCCESS;
// }
__attribute__ ((alias("ccm_auth_crypt_get_result")));

/** @} */
